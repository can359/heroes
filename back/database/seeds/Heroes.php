<?php

use Illuminate\Database\Seeder;

class Heroes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $heroes = [
            array("name" => "Polaris"),
            array("name" => "Esme Frost"),
            array("name" => "Sophie Frost"),
            array("name" => "Phoebe Frost"),
            array("name" => "Blink"),
            array("name" => "Thunderbird"),
            array("name" => "Sage"),
            array("name" => "Rogue"),
            array("name" => "Magneto"),
            array("name" => "Jean Grey"),
            array("name" => "Mystique"),
            array("name" => "Ciclope"),
            array("name" => "Tormenta"),
            array("name" => "Charles Xavier"),
            array("name" => "Psylocke"),
            array("name" => "Bestia"),
            array("name" => "Quicksilver"),
            array("name" => "Nightcrawler"),
            array("name" => "Havok"),
            array("name" => "Gambito")
        ];

        foreach ($heroes as $hero) {
            DB::table('heroes')->insert(["name" => $hero["name"]]);
        }
    }
}
