<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Heroes extends Model
{
    /**
     * Crea los heroes
     * 
     * @param string $name 
     */
    function create ($name)
    {
        DB::table('heroes')
            ->insert(['name' => $name]);
    }

    /**
     * Muestra todos los heroes de la base de datos
     * 
     * @return array $var
     */
    function showList ()
    {
        $var = DB::table('heroes')
                ->select('id', 'name')
                ->orderBy('id', 'asc')
                ->get();

        return $var;
    }

    /**
     * Busca heroe por id
     * 
     * @param number $id 
     * @return array $var
     */
    function findBy ($id)
    {
        $var = DB::table('heroes')
                ->select('id', 'name')
                ->where('id', '=', $id)
                ->get();

        return $var;
    }

    /**
     * Muestra los heroes que contengan la letra ingresada
     * 
     * @param string $name 
     * @return array $var
     */
    function findByName ($name)
    {
        $var = DB::table('heroes')
                ->select('id', 'name')
                ->where('name', 'like', ucwords($name) . '%')
                ->get();

        return $var;
    }

    /**
     * Busca heroe por nombre (funcion hecha solo para no repetir heroes)
     * 
     * @param string $name 
     * @return array $var
     */
    function searchName ($name)
    {
        $var = DB::table('heroes')
                ->select('id', 'name')
                ->where('name', '=', $name)
                ->get();

        return $var;
    }

    /**
     * Modifica heroe especifico
     * 
     * @param number $id 
     * @param string $name 
     * @return array $var
     */
    function updateHero ($id, $name)
    {
        DB::table('heroes')
            ->where('id', $id)
            ->update(['name' => $name]);
    }

    /**
     * Borra el heroe con el id
     * 
     * @param number $id
     */
    function deleteHero ($id)
    {
       DB::table('heroes')
            ->where('id', $id)
            ->delete();
    }


}
