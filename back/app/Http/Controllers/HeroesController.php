<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Heroes;

class HeroesController extends Controller
{
    /**
     * @var any $heroes
     */
    private $heroes;

    /**
     * Instancia $heroes con la clase Heroes()
     * 
     */
    function __construct()
    {
        $this->heroes = new Heroes();
    }

    /**
     * Funcion que moldea los datos de entrada para que los heroes no se repitan y crearlos
     * 
     * @param Request $request 
     * @return json
     */
    function create (Request $request)
    {
        $name = ucwords($request->name);

        $response = self::searchNameToSave($name);

        $original = $response->original;

        foreach ($original as $key => $value) {
            $var[$key] = $value;
        }

        if ($var["message"] == "true") {
            return response()->json([
                "message" => "hero exists"
            ]);
        }
        else {
            $this->heroes->create($name);

            return response()->json([
                "message" => "added hero:" . $name
            ]);
        }
        
    }

    /**
     * Funcion encargada de mostrar los heroes
     * 
     * @return json
     */
    function showList ()
    {
        $response = $this->heroes->showList();

        if (count($response) == 0) {
            return response()->json([
                "message" => "no heroes registered"
            ]);
        }
        else {
            return response()->json($response);    
        }
        
    }

    /**
     * Funcion encargada de buscar heroe por id
     * 
     * @param Request $request 
     * @return json
     */
    function findBy (Request $request)
    {
        $id = $request["id"];

        $response = $this->heroes->findBy($id);

        if (count($response) != 0) {
            return response()->json($response);    
        }
        
    }

    /**
     * Funcion encargada de buscar los heroes por nombre
     * 
     * @param string $name 
     * @return json
     */
    function findByNames ($name) {
        $response = $this->heroes->findByName($name);

        if (count($response) != 0) {
            return response()->json($response);
        }
    }

    /**
     * Funcion encargada de verificar si el heroe existe antes de crearlo
     * 
     * @param string $name 
     * @return json
     */
    function searchNameToSave ($name) {
        $response = $this->heroes->searchName($name);

        if (count($response) != 0) {
            return response()->json([
                "message" => "true"
            ]);
        }
        else{
            return response()->json([
                "message" => "false"
            ]);
        }
    }

    /**
     * Funcion encargada de modificar los heroes
     * 
     * @param Request $request 
     * @return json
     */
    function update (Request $request)
    {
        $auxRequest = $request->all();
        
        foreach ($auxRequest[0] as $key => $value) {
            $data[$key] = $value;
        }

        $name = ucwords($data["name"]);

        $this->heroes->updateHero($data["id"], $name);

        return response()->json([
            "message" => "updated hero id=" . $data["id"]
        ]);

    }

    /**
     * Funcion encargada de eliminar los heroes
     * 
     * @param Request $request 
     * @return json
     */
    function destroy (Request $request)
    {
        $id = $request->id;

        $this->heroes->deleteHero($id);

        return response()->json([
            "message" => "deleted hero id=" . $id
        ]);
    }


}
