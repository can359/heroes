<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ruta para obtener todos los heroes
route::get('/api/heroes', 'HeroesController@showList');

// Ruta para obtener heroe especifico
route::get('/api/heroes/{id}', 'HeroesController@findBy')->where('id', '[0-9]+');

// Ruta para obtener los heroes por letra o nombre
route::get('/api/heroes/{name}', 'HeroesController@findByNames')->where('name', '[a-zA-z\s]+');

// Ruta para crear heroe
route::post('/api/heroes', 'HeroesController@create');

// Ruta para modificar heroe
route::put('/api/heroes', 'HeroesController@update');

// Ruta para eliminar heroe
route::delete('/api/heroes', 'HeroesController@destroy');