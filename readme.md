# Tour of Heroes

_Este proyecto esta basado en el Tour de Heroes de Angular, agregandole el servicio hecho en laravel._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener las herramientas necesarias para el funcionamiento del proyecto en tu máquina local con propósitos de pruebas_

### Pre-requisitos 📋

_Es necesario tener instalado [Composer](https://getcomposer.org/) y [Node.js](https://nodejs.org/en/)_

### Instalación 🔧

_Pasos a ejecutar para descargar y alistar el proyecto_

_1: Descargar_

```
git clone https://gitlab.com/can359/heroes.git
```

_2: Descargar librerias_

_Back_

```
cd back
composer install
```

_Front_

```
cd front
npm install
```
## Poniendo en marcha ⚙️

_Para poner en marcha los frameworks_

### Back

```
cd back
```

_Hacer una copia del archivo ".env.example" y llamarlo ".env"_

**Para Windows**

```
copy .env.example .env
```

**Para Linux**

```
cp .env.example .env
```

_Luego de tener el archivo .env_

```
php artisan key:generate
```

_Configurar la Base de Datos en el mismo archivo ".env", dependiendo de la Base de Datos que se desee utilizar_

```
DB_CONNECTION=extencionBD(pgsql o mysql)
DB_HOST=127.0.0.1
DB_PORT=puertoBD(5432 o 3306)
DB_DATABASE=nombreBD
DB_USERNAME=usuarioBD
DB_PASSWORD=contraseñaBD
```
_Establecida la configuracion, se procede a crear las tablas en la base de datos que se usaran para el funcionamiento de la aplicacion_

```
php artisan migrate
```

_Existe un archivo con heroes ya cargados listos para ser ingresados a la Base de Datos, si deseas hacerlo ejecuta el comando_

```
php artisan db:seed
```

**Iniciar back**

```
php artisan serve
```

### Front

```
cd front
```

_Para iniciar la aplicacion y que se abra automaticamente en el navegador por defecto_

```
ng serve --open
```

_Solo iniciar la aplicacion. se tendra que colocar la direccion [localhost:4200](http://localhost:4200)_

```
ng serve
```

## Aplicacion funcionando ⚙️

_Llegado a este punto la aplicacion debe de estar funcionando..._

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Laravel](https://laravel.com/) - El framework usado para el servicio
* [Angular](https://angular.io/) - El framework usado para el cliente

## Autor ✒️

* **Javier Martinez** - *Desarrollo y Documentación*

---
⌨️ con ❤️ por **Javier Martinez** 😊