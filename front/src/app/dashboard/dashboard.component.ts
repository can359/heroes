import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];

  message;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(
        result => {
          this.message = result;
          if (this.message.message) {
            this.heroService.log(this.message.message);
          }
          else {
            this.heroes = result.slice(0, 4);
            this.heroService.log("fetched heroes");
          }
        }
     );
        

  }
}