import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];

  message;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
    .subscribe(
      result => {
          this.message = result;
          if (this.message.message) {
            this.heroService.log(this.message.message);
          }
          else {
            this.heroes = result;
            this.heroService.log("fetched heroes");
          }
      }
    );
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(
        result => {
          this.message = result;
          this.heroService.log(this.message.message);
        }
      );
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe(
      result => {
        this.message = result;
        this.heroService.log(this.message.message);
      }
    );
  }
}